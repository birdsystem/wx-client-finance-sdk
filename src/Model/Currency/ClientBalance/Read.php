<?php

namespace WarehouseX\ClientFinance\Model\Currency\ClientBalance;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Currency.
 */
class Read extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;
}
