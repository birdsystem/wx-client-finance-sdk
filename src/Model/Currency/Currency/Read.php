<?php

namespace WarehouseX\ClientFinance\Model\Currency\Currency;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Currency.
 */
class Read extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string
     */
    public $symbol = null;

    /**
     * @var string|null
     */
    public $htmlCode = null;
}
