<?php

namespace WarehouseX\ClientFinance\Model\ClientStatementRecord\ClientStatementRecordInput\ClientStatementRecord;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientStatementRecord.
 */
class Write extends AbstractModel
{
    /**
     * @var float
     */
    public $amount = null;

    /**
     * @var string|null
     */
    public $recordCode = null;

    /**
     * @var string
     */
    public $clientBalance = null;

    /**
     * @var string|null
     */
    public $note = null;
}
