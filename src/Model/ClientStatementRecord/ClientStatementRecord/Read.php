<?php

namespace WarehouseX\ClientFinance\Model\ClientStatementRecord\ClientStatementRecord;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientStatementRecord.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $userId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $amount = null;

    /**
     * @var string|null
     */
    public $recordCode = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    public $clientBalance = null;
}
