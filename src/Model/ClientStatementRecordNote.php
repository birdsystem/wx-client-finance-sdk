<?php

namespace WarehouseX\ClientFinance\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientStatementRecordNote.
 */
class ClientStatementRecordNote extends AbstractModel
{
    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $clientStatementRecord = null;
}
