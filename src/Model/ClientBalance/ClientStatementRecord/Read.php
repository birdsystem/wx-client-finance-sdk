<?php

namespace WarehouseX\ClientFinance\Model\ClientBalance\ClientStatementRecord;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientBalance.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    public $currencyCode = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
