<?php

namespace WarehouseX\ClientFinance\Model\ClientBalance\ClientBalance;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientBalance.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    public $client = null;

    /**
     * @var string
     */
    public $balance = null;

    /**
     * @var string
     */
    public $availableBalance = null;

    public $currencyCode = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
