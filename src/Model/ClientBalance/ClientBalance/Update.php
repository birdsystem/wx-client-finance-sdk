<?php

namespace WarehouseX\ClientFinance\Model\ClientBalance\ClientBalance;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientBalance.
 */
class Update extends AbstractModel
{
    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
