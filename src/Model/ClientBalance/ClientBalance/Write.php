<?php

namespace WarehouseX\ClientFinance\Model\ClientBalance\ClientBalance;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientBalance.
 */
class Write extends AbstractModel
{
    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $currencyCode = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
