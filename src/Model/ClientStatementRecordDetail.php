<?php

namespace WarehouseX\ClientFinance\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientStatementRecordDetail.
 */
class ClientStatementRecordDetail extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $amount = '0.0000';

    /**
     * @var string|null
     */
    public $clientStatementRecord = null;
}
