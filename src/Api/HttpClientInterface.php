<?php

namespace WarehouseX\ClientFinance\Api;

use Psr\Http\Client\ClientInterface as BaseClass;

interface HttpClientInterface extends BaseClass
{
}
