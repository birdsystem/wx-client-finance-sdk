<?php

namespace WarehouseX\ClientFinance\Api;

use WarehouseX\ClientFinance\Model\ClientBalance\ClientBalance\Read as Read;
use WarehouseX\ClientFinance\Model\ClientBalance\ClientBalance\Update as Update;
use WarehouseX\ClientFinance\Model\ClientBalance\ClientBalance\Write as Write;

class ClientBalance extends AbstractAPI
{
    /**
     * Retrieves the collection of ClientBalance resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'currencyCode'	string
     *                       'currencyCode[]'	array
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientBalanceCollection',
        'GET',
        'api/client-finance/client_balances',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ClientBalance resource.
     *
     * @param Write $Model The new ClientBalance resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postClientBalanceCollection',
        'POST',
        'api/client-finance/client_balances',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ClientBalance resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getClientBalanceItem',
        'GET',
        "api/client-finance/client_balances/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ClientBalance resource.
     *
     * @param string $id    Resource identifier
     * @param Update $Model The updated ClientBalance resource
     *
     * @return Read
     */
    public function putItem(string $id, Update $Model): Read
    {
        return $this->request(
        'putClientBalanceItem',
        'PUT',
        "api/client-finance/client_balances/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ClientBalance resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientBalanceItem',
        'DELETE',
        "api/client-finance/client_balances/$id",
        null,
        [],
        []
        );
    }
}
