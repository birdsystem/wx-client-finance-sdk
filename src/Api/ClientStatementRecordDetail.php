<?php

namespace WarehouseX\ClientFinance\Api;

use WarehouseX\ClientFinance\Model\ClientStatementRecordDetail as ClientStatementRecordDetailModel;

class ClientStatementRecordDetail extends AbstractAPI
{
    /**
     * Retrieves the collection of ClientStatementRecordDetail resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'type'	string
     *                       'type[]'	array
     *                       'clientStatementRecord'	string
     *                       'clientStatementRecord[]'	array
     *                       'amount[between]'	string
     *                       'amount[gt]'	string
     *                       'amount[gte]'	string
     *                       'amount[lt]'	string
     *                       'amount[lte]'	string
     *                       'order[id]'	string
     *
     * @return ClientStatementRecordDetailModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientStatementRecordDetailCollection',
        'GET',
        'api/client-finance/client_statement_record_details',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ClientStatementRecordDetail resource.
     *
     * @param ClientStatementRecordDetailModel $Model The new
     *                                                ClientStatementRecordDetail resource
     *
     * @return ClientStatementRecordDetailModel
     */
    public function postCollection(ClientStatementRecordDetailModel $Model): ClientStatementRecordDetailModel
    {
        return $this->request(
        'postClientStatementRecordDetailCollection',
        'POST',
        'api/client-finance/client_statement_record_details',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ClientStatementRecordDetail resource.
     *
     * @param string $id Resource identifier
     *
     * @return ClientStatementRecordDetailModel|null
     */
    public function getItem(string $id): ?ClientStatementRecordDetailModel
    {
        return $this->request(
        'getClientStatementRecordDetailItem',
        'GET',
        "api/client-finance/client_statement_record_details/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ClientStatementRecordDetail resource.
     *
     * @param string                           $id    Resource identifier
     * @param ClientStatementRecordDetailModel $Model The updated
     *                                                ClientStatementRecordDetail resource
     *
     * @return ClientStatementRecordDetailModel
     */
    public function putItem(string $id, ClientStatementRecordDetailModel $Model): ClientStatementRecordDetailModel
    {
        return $this->request(
        'putClientStatementRecordDetailItem',
        'PUT',
        "api/client-finance/client_statement_record_details/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ClientStatementRecordDetail resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientStatementRecordDetailItem',
        'DELETE',
        "api/client-finance/client_statement_record_details/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the ClientStatementRecordDetail resource.
     *
     * @param string $id Resource identifier
     *
     * @return ClientStatementRecordDetailModel
     */
    public function patchItem(string $id): ClientStatementRecordDetailModel
    {
        return $this->request(
        'patchClientStatementRecordDetailItem',
        'PATCH',
        "api/client-finance/client_statement_record_details/$id",
        null,
        [],
        []
        );
    }
}
