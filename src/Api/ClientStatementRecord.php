<?php

namespace WarehouseX\ClientFinance\Api;

use WarehouseX\ClientFinance\Model\ClientStatementRecord\ClientStatementRecord\Read as Read;
use WarehouseX\ClientFinance\Model\ClientStatementRecord\ClientStatementRecordInput\ClientStatementRecord\Put as Put;
use WarehouseX\ClientFinance\Model\ClientStatementRecord\ClientStatementRecordInput\ClientStatementRecord\Write as Write;

class ClientStatementRecord extends AbstractAPI
{
    /**
     * Retrieves the collection of ClientStatementRecord resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'userId'	integer
     *                       'userId[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'type'	string
     *                       'type[]'	array
     *                       'recordCode'	string
     *                       'recordCode[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'amount[between]'	string
     *                       'amount[gt]'	string
     *                       'amount[gte]'	string
     *                       'amount[lt]'	string
     *                       'amount[lte]'	string
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[createTime]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientStatementRecordCollection',
        'GET',
        'api/client-finance/client_statement_records',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ClientStatementRecord resource.
     *
     * @param Write $Model The new ClientStatementRecord resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postClientStatementRecordCollection',
        'POST',
        'api/client-finance/client_statement_records',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ClientStatementRecord resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getClientStatementRecordItem',
        'GET',
        "api/client-finance/client_statement_records/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ClientStatementRecord resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated ClientStatementRecord resource
     *
     * @return Read
     */
    public function putItem(string $id, Put $Model): Read
    {
        return $this->request(
        'putClientStatementRecordItem',
        'PUT',
        "api/client-finance/client_statement_records/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ClientStatementRecord resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientStatementRecordItem',
        'DELETE',
        "api/client-finance/client_statement_records/$id",
        null,
        [],
        []
        );
    }
}
