<?php

namespace WarehouseX\ClientFinance\Api;

use WarehouseX\ClientFinance\Model\ClientStatementRecordNote as ClientStatementRecordNoteModel;

class ClientStatementRecordNote extends AbstractAPI
{
    /**
     * Retrieves the collection of ClientStatementRecordNote resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'clientStatementRecord'	string
     *                       'clientStatementRecord[]'	array
     *
     * @return ClientStatementRecordNoteModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientStatementRecordNoteCollection',
        'GET',
        'api/client-finance/client_statement_record_notes',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ClientStatementRecordNote resource.
     *
     * @param ClientStatementRecordNoteModel $Model The new ClientStatementRecordNote
     *                                              resource
     *
     * @return ClientStatementRecordNoteModel
     */
    public function postCollection(ClientStatementRecordNoteModel $Model): ClientStatementRecordNoteModel
    {
        return $this->request(
        'postClientStatementRecordNoteCollection',
        'POST',
        'api/client-finance/client_statement_record_notes',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ClientStatementRecordNote resource.
     *
     * @param string $clientStatementRecord Resource identifier
     *
     * @return ClientStatementRecordNoteModel|null
     */
    public function getItem(string $clientStatementRecord): ?ClientStatementRecordNoteModel
    {
        return $this->request(
        'getClientStatementRecordNoteItem',
        'GET',
        "api/client-finance/client_statement_record_notes/$clientStatementRecord",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ClientStatementRecordNote resource.
     *
     * @param string                         $clientStatementRecord Resource identifier
     * @param ClientStatementRecordNoteModel $Model                 The updated
     *                                                              ClientStatementRecordNote resource
     *
     * @return ClientStatementRecordNoteModel
     */
    public function putItem(string $clientStatementRecord, ClientStatementRecordNoteModel $Model): ClientStatementRecordNoteModel
    {
        return $this->request(
        'putClientStatementRecordNoteItem',
        'PUT',
        "api/client-finance/client_statement_record_notes/$clientStatementRecord",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ClientStatementRecordNote resource.
     *
     * @param string $clientStatementRecord Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $clientStatementRecord): mixed
    {
        return $this->request(
        'deleteClientStatementRecordNoteItem',
        'DELETE',
        "api/client-finance/client_statement_record_notes/$clientStatementRecord",
        null,
        [],
        []
        );
    }

    /**
     * Updates the ClientStatementRecordNote resource.
     *
     * @param string $clientStatementRecord Resource identifier
     *
     * @return ClientStatementRecordNoteModel
     */
    public function patchItem(string $clientStatementRecord): ClientStatementRecordNoteModel
    {
        return $this->request(
        'patchClientStatementRecordNoteItem',
        'PATCH',
        "api/client-finance/client_statement_record_notes/$clientStatementRecord",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a ClientStatementRecord resource.
     *
     * @param string $id ClientStatementRecord identifier
     *
     * @return ClientStatementRecordNoteModel|null
     */
    public function api_client_statement_records_client_statement_record_note_get_subresourceClientStatementRecordSubresource(string $id): ?ClientStatementRecordNoteModel
    {
        return $this->request(
        'api_client_statement_records_client_statement_record_note_get_subresourceClientStatementRecordSubresource',
        'GET',
        "api/client-finance/client_statement_records/$id/client_statement_record_note",
        null,
        [],
        []
        );
    }
}
