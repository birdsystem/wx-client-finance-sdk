<?php

namespace WarehouseX\ClientFinance\Api;

use WarehouseX\ClientFinance\Model\Currency\Currency\Read as Read;
use WarehouseX\ClientFinance\Model\Currency\Currency\Write as Write;

class Currency extends AbstractAPI
{
    /**
     * Retrieves the collection of Currency resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'code[]'	array
     *                       'name'	string
     *                       'name[]'	array
     *                       'symbol'	string
     *                       'symbol[]'	array
     *                       'htmlCode'	string
     *                       'htmlCode[]'	array
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getCurrencyCollection',
        'GET',
        'api/client-finance/currencies',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Currency resource.
     *
     * @param Write $Model The new Currency resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postCurrencyCollection',
        'POST',
        'api/client-finance/currencies',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Currency resource.
     *
     * @param string $code Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $code): ?Read
    {
        return $this->request(
        'getCurrencyItem',
        'GET',
        "api/client-finance/currencies/$code",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Currency resource.
     *
     * @param string $code  Resource identifier
     * @param Write  $Model The updated Currency resource
     *
     * @return Read
     */
    public function putItem(string $code, Write $Model): Read
    {
        return $this->request(
        'putCurrencyItem',
        'PUT',
        "api/client-finance/currencies/$code",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Currency resource.
     *
     * @param string $code Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $code): mixed
    {
        return $this->request(
        'deleteCurrencyItem',
        'DELETE',
        "api/client-finance/currencies/$code",
        null,
        [],
        []
        );
    }
}
