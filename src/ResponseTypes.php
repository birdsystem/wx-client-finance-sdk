<?php

namespace WarehouseX\ClientFinance;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getClientBalanceCollection' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientBalance\\ClientBalance\\Read[]',
        ],
        'postClientBalanceCollection' => [
            '201.' => 'WarehouseX\\ClientFinance\\Model\\ClientBalance\\ClientBalance\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientBalanceItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientBalance\\ClientBalance\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientBalanceItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientBalance\\ClientBalance\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientBalanceItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientStatementRecordDetailCollection' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordDetail[]',
        ],
        'postClientStatementRecordDetailCollection' => [
            '201.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientStatementRecordDetailItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientStatementRecordDetailItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientStatementRecordDetailItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchClientStatementRecordDetailItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientStatementRecordNoteCollection' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordNote[]',
        ],
        'postClientStatementRecordNoteCollection' => [
            '201.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordNote',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientStatementRecordNoteItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordNote',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientStatementRecordNoteItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordNote',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientStatementRecordNoteItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchClientStatementRecordNoteItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordNote',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientStatementRecordCollection' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecord\\ClientStatementRecord\\Read[]',
        ],
        'postClientStatementRecordCollection' => [
            '201.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecord\\ClientStatementRecord\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientStatementRecordItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecord\\ClientStatementRecord\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientStatementRecordItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecord\\ClientStatementRecord\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientStatementRecordItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_client_statement_records_client_statement_record_note_get_subresourceClientStatementRecordSubresource' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\ClientStatementRecordNote',
        ],
        'getCurrencyCollection' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\Currency\\Currency\\Read[]',
        ],
        'postCurrencyCollection' => [
            '201.' => 'WarehouseX\\ClientFinance\\Model\\Currency\\Currency\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getCurrencyItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\Currency\\Currency\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putCurrencyItem' => [
            '200.' => 'WarehouseX\\ClientFinance\\Model\\Currency\\Currency\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteCurrencyItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
